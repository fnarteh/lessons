<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       
      $this->call(CompanyTableSeeder::class);
      $this->call(UserTableSeeder::class);
      //$this->call(EmailTableSeeder::class);
      
    }
}
