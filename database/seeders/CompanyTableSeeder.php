<?php

namespace Database\Seeders;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('company')->delete();
        DB::table('companies')->insert([
            ['id' => 1,
            'name' => ('Microsoft'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ],

            ['id' => 2,
            'name' => ('Apple') ,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ],

            ['id' => 3,
            'name' => ('Serene'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ],

            ['id' => 4,
            'name' => ('Happy Home'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ],

            ['id' => 5,
            'name' => ('The Cobalt Partners'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ],

            ['id' => 6,
            'name' => ('Acqua Safari'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),],

            ['id' => 7,
            'name' => ('Amazon'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),]
        ]);
    }
}
