<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([

            ['id' => 1,
            'company_id'=>1,
            'password' => 'password',
            'name' => 'Collins Mark',
            'email' => 'collins@yahoo.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ],
            
            [ 'id' => 2,
            'company_id'=>2,
            'password' => 'password',
            'name' => 'Mike Owusu',
            'email' => 'mike@yahoo.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ],
            
            [ 'id' => 3,
            'company_id'=>3,
            'password' => 'password',
            'name' => 'Jane Attah',
            'email' => 'jane@serene.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

            ],
            
            ['id' => 4,
            'company_id'=>4,
            'password' => 'password',
            'name' => 'Paul Arthur',
            'email' => 'paul@gmail.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ],

            ['id' => 5,
            'company_id'=>5,
            'password' => 'password',
            'name' => 'Frank Armah',
            'email' => 'frank@gmail.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ],

            ['id' => 6,
            'company_id'=>6,
            'password' => 'password',
            'name' => 'Patricia Barns',
            'email' => 'pat@gmail.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ],
           

            ['id' => 7,
            'company_id'=>7,
            'password' => 'password',
            'name' => 'Peter Ansah',
            'email' => 'peter@gmail.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),],


            ['id' => 8,
            'company_id'=>1,
            'password' => 'password',
            'name' => 'Karen Clare',
            'email' => 'clare@outlook.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),],


            ['id' => 9,
            'company_id'=>2,
            'password' => 'password',
            'name' => 'Jame Anann',
            'email' => 'jann@gmail.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),],

            ['id' => 10,
            'company_id'=>5,
            'password' => 'password',
            'name' => 'Floxy Narteh',
            'email' => 'fnarteh@thecobaltpartners.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),],


            ['id' => 11,
            'company_id'=>5,
            'password' => 'password',
            'name' => 'Lilly Mensah',
            'email' => 'mensahlilly@thecobaltpartners.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),],

            ['id' => 12,
            'company_id'=>3,
            'password' => 'password',
            'name' => 'Kwesi Bonsrah',
            'email' => 'bonsrah@serene.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),],


            ['id' => 13,
            'company_id'=>4,
            'password' => 'password',
            'name' => 'Drake Mark',
            'email' => 'dmark@gmail.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ],


            ['id' => 14,
            'company_id'=>7,
            'password' => 'password',
            'name' => 'Selina Ogoo',
            'email' => 'ogoos@gmail.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ],


            ['id' => 15,
            'company_id'=>6,
            'password' => 'password',
            'name' => 'Patrick Jones',
            'email' => 'jonesp@gmail.com',
            'email_verified_at' => '2021-02-02',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            ],
            
        ]);
    }
}
